var B = 5;
var K = 2;
var soma1 = B+K

function soma(a,b){
  var total;
  total = a+b;
  return total;
}

function fat(n){
  var resultado = 1;
  
  if (n <= 1){
    return 1;
  }
  resultado = n*fat(n-1);
  return resultado;
}

//console.log(fat(5));

function calcularAnaliseCombinatoria(){
  //alert('ok');
  let B = document.getElementById("B").value
  //alert(N);
  let K = document.getElementById("K").value
  //console.log(P);
  
  let arranj;
  let res = document.getElementById("resultado")
  
  if ((B <= 1) || (K <= 1)){
    res.innerText = 'CUIDADO: B e K devem ser > 1';
  } else {
    arranj = fat(B)/fat(K);
    if (arranj == 1){
      res.innerText = 'Resultado: '+arranj+' arranjo';
    } else {
      res.innerText = 'Resultado: '+arranj+' arranjos';
    }
  }
}